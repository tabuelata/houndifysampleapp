import sys 
import houndify
import time
import wave

## CONFIGURATION ==============================================

# Client
CLIENT_ID  = '' # FILL THIS IN
CLIENT_KEY = '' # FILL THIS IN
USER_ID = '5G-test-user'

# Simulate real time
SIMULATE_REAL_TIME = False

# Use Speex compression
USE_SPEEX = False

# Proxy
USE_PROXY = False
PROXY_HOST = None
PROXY_PORT = None
PROXY_HEADERS = None

# Request info params
requestInfo = {
  "Latitude": 40.706564,
  "Longitude": -74.549393,
  "InputLanguageEnglishName": 'English',
  "OutputLanguageEnglishName": 'English',
  "ServerDeterminesEndOfAudio": True,
  "UnitPreference": 'US'
}

## SCRIPT =====================================================

CHUNKSIZE = 2058 # Size of audio chunks

# Finish the query if the same transcript
# is received that many times in a row
FINISH_QUERY_FACTOR = 12

# Utility function
def green(s):
    return "\033[92m{}\033[00m".format(s)
def cyan(s):
    return "\033[96m{}\033[00m".format(s)

# Check arguments
if len(sys.argv) != 2:
    print('Please provide a path to an audio file as an argument')
    exit(-1)

audio_file = sys.argv[1]

# Define listener and callbacks methods
# Here you can define what happens as partial
# Transcripts are received while the audio streams
class CustomListener(houndify.HoundListener):
    transcript = ''
    unchanged_count = 0

    def __init__(self):
        pass

    def onPartialTranscript(self, transcript):
        print(">", green(transcript), ' ', end='')
        print_elapsed()
        if transcript == self.transcript:
            self.unchanged_count += 1
        else:
            self.unchanged_count = 0
            self.transcript = transcript

    def onFinalResponse(self, response):
        #print("Final response: " + str(response))
        pass

    def onError(self, err):
        print("Error " + str(err))


# Create the Houndify audio client
client = houndify.StreamingHoundClient(CLIENT_ID, CLIENT_KEY, USER_ID, requestInfo, useSpeex=USE_SPEEX)

# Function to print time elapsed
start_time = time.time()
def print_elapsed():
    print("%s seconds elapsed" % round((time.time() - start_time), 3))

# Open the audio file
print('Opening', audio_file)
audio = wave.open(audio_file)
if audio.getsampwidth() != 2:
    raise Exception(audio_file + ': wrong sample width (must be 16-bit)')
if audio.getframerate() != 8000 and audio.getframerate() != 16000:
    raise Exception(audio_file + ': unsupported sampling frequency (must be either 8 or 16 khz)')
if audio.getnchannels() != 1:
    raise Exception(audio_file + ': must be single channel (mono)')
client.setSampleRate(audio.getframerate())
listener = CustomListener()

# Start streaming the audio chunks
client.start(listener)
print(cyan('start() completed'), end=' ')
print_elapsed()

while True:
    try:
        chunk_start = time.time()
        samples = audio.readframes(CHUNKSIZE)
        chunk_duration = len(samples) / (audio.getframerate() * audio.getsampwidth())
        if len(samples) == 0: break
        if client.fill(samples): break
        if listener.unchanged_count > FINISH_QUERY_FACTOR:
            client.audioFinished = True
            break
        if SIMULATE_REAL_TIME:
            time.sleep(chunk_duration - time.time() + chunk_start)
    except Exception as e:
        print(e)
        break
# Call finish to end the query and get the final result
result = client.finish()
# Extract some info from the response and print to screen
print('QueryID:', result['QueryID'])
response = result['AllResults'][0]['WrittenResponse']
print(cyan(response), end=' ')
print_elapsed()
audio.close()