# Houndify Sample App for Verizon

This app was written to allow you to quickly run audio queries using the Houndify platform.

### Notes

This app uses the Python Houndify SDK, which you need to install (see installation section).
The script needs to be modified before you can run it (see configuration section).

## Installation

This script requires Python 3 and several libraries. Make sure you have Python 3 installed and install the following dependency:

```
pip3 install houndify
```

You may need to use `sudo` based on how your Python environment is set up.

## Configuration

You will need to edit the script and enter your client ID and client key. These are found on the Houndify.com website under the "Overview & API Keys" tab.

Edit the variables at the top of the file by copy/pasting those values into the `CLIENT_ID` and `CLIENT_KEY` variables.

## Execution

The script's input is an audio file with a recorded voice query such as "what is 5G?"
I have provided a sample file. You will need to record the other ones.

The script is run like this:

```
python3 run_audio_query.py what_is_5g.wav
```

Timing information will also be printed on the screen. You can use that to compare run time in various environments.

## Using a proxy

To route the queries through a proxy server you must edit at least three fields in the script:

 - `USE_PROXY`: set this to `True`
 - `PROXY_HOST`: set this to the IP or hostname of your proxy server
 - `PROXY_PORT`: set this to the port on which your proxy is running on the host above
 - Optional: `PROXY_HEADERS`: set this if you need to pass parameters to your proxy, otherwise leave as-is

## Other configuration

This program uses a pre-recorded audio file. That means we are able to send the chunks
one after another wihtout waiting. In real life we would read the audio from a microphone
and therefore there would be some delays between the various `fill()` calls.

If you want to simulate real-time streaming you may set `SIMULATE_REAL_TIME` to `True`.

If you want to use Speex compression instead of sending the raw WAV data, set `USE_SPEEX` to `True`.

## Recording more audio

An easy way to create more audio files is to use the Audacity application which
is free and open source: https://www.audacityteam.org/

 - Open Audacity
 - Hit the red record button and speak your intent
 - Hit the stop button
 - Set the project rate to 16000 Hz at the bottom left corner
 - Go the the "File > Export > Export as WAV" menu
 - Select "Signed 16-bit PCM"